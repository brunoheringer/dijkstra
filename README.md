# README #
## Dijkstra
Dijkstra Algoritm

This project was built to answer the test. (December / 2017).  
**Importants informations about this project:**
> I didn't write unit tests. (But I'll still do it later)  

> I used a classic implementation of the Djikstra Algorithm, but I already make many changes in this algoríthm including making it a directional graph.  

> Missing exercises: 6,7,10


### How do I get set up? ###   
This project uses only C#. So, with a VS(https://www.visualstudio.com/pt-br/) you can run the console app. 

### Who do I talk to? ###
* Bruno Heringer | 31 98822-2391

### To Do ###
* Create "Count possibles Routes"
* I still have some technical debts in this project. For example, create right layers and right files of classes, and some importants refactorins.
* Create versioning


### Examples ###
Some photos of this aplication:
Main Screem
![1.png](https://bytebucket.org/brunoheringer/dijkstra/raw/d769db5acb5db232e90a1f01304ddf66e5ae557b/Prova/Example.png?token=4bca21f586734d55f4b1319acdc4ceef1974b9cd)  
------
