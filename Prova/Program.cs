﻿using System;
using System.Collections.Generic;

namespace Dijkstra
{
    class Graph
    {
        private Dictionary<char, Dictionary<char, int>> vertices = new Dictionary<char, Dictionary<char, int>>();

        public void Add_vertex(char name, Dictionary<char, int> edges)
        {
            vertices[name] = edges;
        }

        private List<char> Shortest_path(char start, char finish)
        {
            var previous = new Dictionary<char, char>();
            var distances = new Dictionary<char, int>();
            var nodes = new List<char>();

            List<char> path = null;

            foreach (var vertex in vertices)
            {
                if (vertex.Key == start)
                {
                    distances[vertex.Key] = 0;
                }
                else
                {
                    distances[vertex.Key] = int.MaxValue;
                }

                nodes.Add(vertex.Key);
            }

            while (nodes.Count != 0)
            {
                nodes.Sort((x, y) => distances[x] - distances[y]);

                var smallest = nodes[0];
                nodes.Remove(smallest);

                if (smallest == finish)
                {
                    path = new List<char>();
                    while (previous.ContainsKey(smallest))
                    {
                        path.Add(smallest);

                        smallest = previous[smallest];
                    }

                    break;
                }

                if (distances[smallest] == int.MaxValue)
                {
                    break;
                }

                foreach (var neighbor in vertices[smallest])
                {
                    var alt = distances[smallest] + neighbor.Value;
                    if (alt < distances[neighbor.Key])
                    {
                        distances[neighbor.Key] = alt;
                        if (vertices[smallest].ContainsKey(neighbor.Key))
                            previous[neighbor.Key] = smallest;
                    }
                }
            }

            return path;
        }

        private char GetSmallerNeighbor(char start)
        {
            var neigbhors = vertices[start];
            var minValue = int.MaxValue;
            char smaller = '-';
            foreach (var item in neigbhors)
            {
                if(item.Value < minValue)
                {
                    minValue = item.Value;
                    smaller = item.Key;
                }
            }
            return smaller;
        }

        public int GetDistance(List<char> routeTrip)
        {
            int distance = 0;

            try
            {
                var routeArray = routeTrip.ToArray();

                for (int i = 0; i < routeArray.Length - 1; i++)
                {
                    var neigbhors = vertices[routeArray[i]];
                    var nextVertexValue = neigbhors[routeArray[i + 1]];

                    distance += nextVertexValue;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return distance;
        }

        public bool VerifyPossibleRoute(List<char> routeTrip)
        {
            try
            {
                var rotaarray = routeTrip.ToArray();

                for (int i = 0; i < rotaarray.Length - 1; i++)
                {
                    var neigbhors = vertices[rotaarray[i]];
                    if (!neigbhors.ContainsKey(rotaarray[i + 1]))
                        return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return true;
        }

        public List<char> GetPossibibleRoute(List<char> routeTrip)
        {
            List<char> route = new List<char> { routeTrip[0] };
            if (routeTrip[0] == routeTrip[routeTrip.Count - 1] && routeTrip.Count == 2)
            {
                routeTrip[1] = GetSmallerNeighbor(routeTrip[0]);
                routeTrip.Add(routeTrip[routeTrip.Count - 2]);
            }
            for (int i = 0; i < routeTrip.Count - 1; i++)
            {
                var bestWay = new List<char>();
                bestWay = Shortest_path(routeTrip[i], routeTrip[i + 1]);
                if (bestWay.Count == 0)
                {
                    return bestWay;
                }
                bestWay.Reverse();
                route.AddRange(bestWay);
            }

            return route;
        }

        public void PrintRoute(List<char> routeTrip)
        {
            if (routeTrip.Count != 0)
            {
                Console.Write("Rota disponível: ");
                routeTrip.ForEach(x => Console.Write(x + "=>"));
                Console.Write(" Finished \n");
            }
            else
            {
                Console.WriteLine("Rota não disponível!");
            }
        }
    }

    class MainClass
    {
        public static Graph graph = new Graph();

        public static void Main()
        {
            InitializeGraph(graph);

            Ex1();
            Ex2();
            Ex3();
            Ex4();
            Ex5();
            Ex8();
            Ex9();
        }

        public static void InitializeGraph(Graph graph)
        {
            graph.Add_vertex('A', new Dictionary<char, int>() { { 'B', 5 }, { 'D', 5 }, { 'E', 7 } });
            graph.Add_vertex('B', new Dictionary<char, int>() { { 'C', 4 } });
            graph.Add_vertex('C', new Dictionary<char, int>() { { 'D', 8 }, { 'E', 2 } });
            graph.Add_vertex('D', new Dictionary<char, int>() { { 'C', 8 }, { 'E', 6 } });
            graph.Add_vertex('E', new Dictionary<char, int>() { { 'B', 3 } });
        }

        public static void ExBase(List<char> solicitedRoute, string description)
        {
            Console.WriteLine(" ############### ");
            Console.WriteLine(description);

            #region Region Possible
            var possible = graph.VerifyPossibleRoute(solicitedRoute);
            if (possible)
                Console.WriteLine("Rota direta existente.");
            else
                Console.WriteLine("Rota direta não existente!");
            #endregion

            var way = graph.GetPossibibleRoute(solicitedRoute);
            graph.PrintRoute(way);

            Console.WriteLine("Distância Total: " + graph.GetDistance(way));
            Console.WriteLine(" ############### \n");
            Console.WriteLine("");
            Console.ReadKey();
        }

        public static void Ex1()
        {
            var solicitedRoute = new List<char> { 'A', 'B', 'C' };
            ExBase(solicitedRoute, "1.A distância da rota A-B-C.");
        }

        public static void Ex2()
        {
            var solicitedRoute = new List<char> { 'A', 'D' };
            ExBase(solicitedRoute, "2. A distância da rota A-D.");
        }

        public static void Ex3()
        {
            var solicitedRoute = new List<char> { 'A', 'D', 'C' };
            ExBase(solicitedRoute, "3. A distância da rota A-D-C.");
        }

        public static void Ex4()
        {
            var solicitedRoute = new List<char> { 'A', 'E', 'B', 'C', 'D' };
            ExBase(solicitedRoute, "4. A distância da rota A-E-B-C-D.");
        }

        public static void Ex5()
        {
            var solicitedRoute = new List<char> { 'A', 'E', 'D' };
            ExBase(solicitedRoute, "5. A distância da rota A-E-D.");
        }

        public static void Ex8()
        {
            var solicitedRoute = new List<char> { 'A', 'C' };
            ExBase(solicitedRoute, "8. O tamanho da menor viagem (em termos de distância) de A para C.");
        }

        public static void Ex9()
        {
            var solicitedRoute = new List<char> { 'B', 'B' };
            ExBase(solicitedRoute, "9. O tamanho da menor viagem (em termos de distância) de B para B.");
        }
    }
}